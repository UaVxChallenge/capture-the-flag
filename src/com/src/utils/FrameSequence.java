package com.src.utils;

import java.util.List;

public class FrameSequence
{
  private int fadeIn;
  private int stay;
  private int fadeOut;
  private int totalTime;
  private List<AnimationFrame> frames;

  public FrameSequence(List<AnimationFrame> frames)
  {
    this.frames = frames;
    for (int i = 0; frames.size() > i; i++) {
      AnimationFrame frame = (AnimationFrame)frames.get(i);
      if (i == 0) {
        this.fadeIn = (frame.getFadeIn() == -1 ? 0 : frame.getFadeIn());
        this.stay += (frame.getStay() == -1 ? 0 : frame.getStay());
        this.stay += (frame.getFadeOut() == -1 ? 0 : frame.getFadeOut());
      } else if (i + 1 == frames.size()) {
        this.stay += (frame.getFadeIn() == -1 ? 0 : frame.getFadeIn());
        this.stay += (frame.getStay() == -1 ? 0 : frame.getStay());
        this.fadeOut = (frame.getFadeOut() == -1 ? 0 : frame.getFadeOut());
      } else {
        this.stay += (frame.getFadeIn() == -1 ? 0 : frame.getFadeIn());
        this.stay += (frame.getStay() == -1 ? 0 : frame.getStay());
        this.stay += (frame.getFadeOut() == -1 ? 0 : frame.getFadeOut());
      }
      this.totalTime += frame.getTotalTime();
    }
  }

  public List<AnimationFrame> getFrames() {
    return this.frames;
  }

  public int size() {
    return this.frames.size();
  }

  public int getFadeIn() {
    return this.fadeIn;
  }

  public int getStay() {
    return this.stay;
  }

  public int getFadeOut() {
    return this.fadeOut;
  }

  public int getTotalTime() {
    return this.totalTime;
  }
}