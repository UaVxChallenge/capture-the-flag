package com.src.utils;

public class AnimationFrame
{
  private String rawText;
  private Object componentText;
  private int fadeIn = -1;
  private int stay = -1;
  private int fadeOut = -1;

  public AnimationFrame(String text, int fadeIn, int stay, int fadeOut) {
    setText(text);
    this.fadeIn = fadeIn;
    this.stay = stay;
    this.fadeOut = fadeOut;
  }

  public String getText() {
    return this.rawText;
  }

  public void setText(String text) {
    this.rawText = text;
    this.componentText = TitleManager.getReflectionManager().getIChatBaseComponent(text);
  }

  public Object getComponentText() {
    return this.componentText;
  }

  public int getFadeIn() {
    return this.fadeIn;
  }

  public void setFadeIn(int fadeIn) {
    this.fadeIn = fadeIn;
  }

  public int getStay() {
    return this.stay;
  }

  public void setStay(int stay) {
    this.stay = stay;
  }

  public int getFadeOut() {
    return this.fadeOut;
  }

  public void setFadeOut(int fadeOut) {
    this.fadeOut = fadeOut;
  }

  public int getTotalTime() {
    return this.fadeIn + this.stay + this.fadeOut;
  }
}
