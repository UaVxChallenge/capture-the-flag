package com.src.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ActionbarTitleObject
implements IActionbarObject
{
private String rawTitle;
private Object title;

public ActionbarTitleObject(String title)
{
  setTitle(title);
}

public void broadcast()
{
  for (Player player : Bukkit.getOnlinePlayers())
    send(player);
}

public void send(Player player)
{
  ActionbarEvent event = new ActionbarEvent(player, this);
  Bukkit.getServer().getPluginManager().callEvent(event);

  if (event.isCancelled()) return;

  TitleManager.getReflectionManager().sendPacket(TitleManager.getReflectionManager().constructActionbarTitlePacket((this.rawTitle.contains("{")) && (this.rawTitle.contains("}")) ? TitleManager.getReflectionManager().getIChatBaseComponent(TextConverter.setVariables(player, this.rawTitle)) : this.title), player);
}

public String getTitle() {
  return this.rawTitle;
}

public void setTitle(String title) {
  this.rawTitle = title;
  this.title = TitleManager.getReflectionManager().getIChatBaseComponent(title);
}
}