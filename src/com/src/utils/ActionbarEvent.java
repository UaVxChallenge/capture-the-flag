package com.src.utils;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ActionbarEvent extends Event
implements Cancellable
{
private static final HandlerList handlers = new HandlerList();
private boolean cancelled;
private Player player;
private ActionbarTitleObject titleObject;

public ActionbarEvent(Player player, ActionbarTitleObject titleObject)
{
  this.player = player;
  this.titleObject = titleObject;
}

public static HandlerList getHandlerList() {
  return handlers;
}

public HandlerList getHandlers()
{
  return handlers;
}

public boolean isCancelled()
{
  return this.cancelled;
}

public void setCancelled(boolean shouldCancel)
{
  this.cancelled = shouldCancel;
}

public Player getPlayer() {
  return this.player;
}

public ActionbarTitleObject getTitleObject() {
  return this.titleObject;
}

public void setTitleObject(ActionbarTitleObject titleObject) {
  this.titleObject = titleObject;
}
}