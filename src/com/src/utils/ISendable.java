package com.src.utils;

import org.bukkit.entity.Player;

public interface ISendable{
    public void broadcast();
    public void send(Player player);
}