package com.src.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.src.CTF;

public class LocationUtils {

	public static Location RedSpawn = new Location(Bukkit.getWorld(CTF.instance.getConfig().getString("RedSpawn.World")), CTF.instance.getConfig().getDouble("RedSpawn.X"), CTF.instance.getConfig().getDouble("RedSpawn.Y"), CTF.instance.getConfig().getDouble("RedSpawn.Z"));
	public static Location BlueSpawn = new Location(Bukkit.getWorld(CTF.instance.getConfig().getString("BlueSpawn.World")), CTF.instance.getConfig().getDouble("BlueSpawn.X"), CTF.instance.getConfig().getDouble("BlueSpawn.Y"), CTF.instance.getConfig().getDouble("BlueSpawn.Z"));
	public static Location Lobby = new Location(Bukkit.getWorld(CTF.instance.getConfig().getString("Lobby.World")), CTF.instance.getConfig().getDouble("Lobby.X"), CTF.instance.getConfig().getDouble("Lobby.Y"), CTF.instance.getConfig().getDouble("Lobby.Z"));


}
