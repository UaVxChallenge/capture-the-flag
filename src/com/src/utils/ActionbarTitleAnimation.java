package com.src.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

public class ActionbarTitleAnimation
implements IAnimation, IActionbarObject
{
private ReflectionManager manager;
private FrameSequence title;

public ActionbarTitleAnimation(FrameSequence title)
{
  this.manager = TitleManager.getReflectionManager();
  this.title = title;
}

public void broadcast()
{
  send(null);
}

public void send(Player player)
{
  Plugin plugin = TitleManager.getPlugin();
  BukkitScheduler scheduler = Bukkit.getScheduler();

  long times = 0L;
  for (AnimationFrame frame : this.title.getFrames()) {
    scheduler.runTaskLaterAsynchronously(plugin, new Task(frame, player), times);
    times += frame.getTotalTime();
  }
}

private class Task implements Runnable {
  private AnimationFrame frame;
  private Player player;

  public Task(AnimationFrame frame, Player player) { this.frame = frame;
    this.player = player;
  }

public void run()
  {
    if (this.player == null)
      for (Player p : Bukkit.getServer().getOnlinePlayers())
        send(p, this.frame);
    else send(this.player, this.frame); 
  }

  private void send(Player p, AnimationFrame frame)
  {
    if (p != null)
      ActionbarTitleAnimation.this.manager.sendPacket(ActionbarTitleAnimation.this.manager.constructActionbarTitlePacket((frame.getText().contains("{")) && (frame.getText().contains("}")) ? ActionbarTitleAnimation.this.manager.getIChatBaseComponent(TextConverter.setVariables(this.player, frame.getText())) : frame.getComponentText()), p);
  }
}
}
