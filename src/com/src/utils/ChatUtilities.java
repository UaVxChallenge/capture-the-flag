package com.src.utils;

import static org.bukkit.ChatColor.BLUE;
import static org.bukkit.ChatColor.BOLD;
import static org.bukkit.ChatColor.RED;
import static org.bukkit.ChatColor.WHITE;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ChatUtilities {

	public static void broadcast(String msg){
		for(Player player : Bukkit.getOnlinePlayers()) {
			player.sendMessage(starter() + msg);
		}
	}
	
	private static String starter(){
		return WHITE + "[" + BLUE + "CTF" + WHITE + "]" + BOLD + " > " + RED;
	}
	
}
