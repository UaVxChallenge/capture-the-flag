package com.src;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.src.classes.Classes;
import com.src.events.PlayerDamage;
import com.src.events.PlayerDeath;
import com.src.events.PlayerJoin;
import com.src.events.RightClickBlock;
import com.src.scoreboards.PregameScores;
import com.src.threads.StartCountdown;
import com.src.utils.LocationUtils;
import com.src.utils.TitleManager;

public class CTF extends JavaPlugin{
	
	public static int startCountdownID;
	public static int ScoreboardPreID;
	public static Plugin instance;
	
	@Override
	public void onEnable() {
		startCountDown();
		
		instance = this;
		
		PregameScores.i = Bukkit.getOnlinePlayers().size();
		
		for(Player p: Bukkit.getOnlinePlayers()){
			PregameScores.ScoreBoardPre(p);
		}
		
		ScoreboardManager();
		
        try {
			TitleManager.load(this);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
        
        for(Player p : Bukkit.getOnlinePlayers()){
        	Classes.inLobbyKit(p);
        	p.teleport(LocationUtils.Lobby);
        }
        
        Bukkit.getServer().getPluginManager().registerEvents(new RightClickBlock(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerDamage(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new Classes(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerDeath(), this);
        
        GameState.setState(GameState.IN_LOBBY);
        Game.setHasStarted(false);
        
		initializeConfig();
		
		Classes.WarriorLore.add(ChatColor.BLUE + "Warriors or Killers");
		Classes.WarriorLore.add(ChatColor.BLUE + "Who is more dangerous?");

		Classes.ArcherLore.add(ChatColor.BLUE + "Archers are the most effective");
		Classes.ArcherLore.add(ChatColor.BLUE + "from long range!");
	}
	
	@Override
	public void onDisable() {
		
		Bukkit.getServer().getScheduler().cancelAllTasks();
		
		Game.respawnBlueFlag();
		Game.respawnRedFlag();
		
	}

	public void initializeConfig(){		
		getConfig().set("Default", " ");
		this.saveConfig();
	}

	public void startCountDown(){
		StartCountdown.timeUntilStart = 15;
		startCountdownID = getServer().getScheduler().scheduleSyncRepeatingTask(this, new StartCountdown(this), 20l, 20l);
	}
	
	public void restartCountdown(){
		Game.stopCountdown();
		startCountDown();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd,
			String label, String[] args) {
		
		if(!(sender instanceof Player)){
			return false;
		}
		
		Player p = (Player) sender;
		if(p.hasPermission("CTF.Admin")){
		if(cmd.getLabel().equalsIgnoreCase("SetBlueSpawn")){
			getConfig().set("BlueSpawn.World", p.getWorld().getName().toString());
			getConfig().set("BlueSpawn.X", p.getLocation().getBlockX());
			getConfig().set("BlueSpawn.Y", p.getLocation().getBlockY());
			getConfig().set("BlueSpawn.Z", p.getLocation().getBlockZ());
			p.sendMessage(ChatColor.GOLD + "Blue Spawn has been set!");
			saveConfig();
			return true;
		}
		}
		
		if(p.hasPermission("CTF.Admin")){
		if(cmd.getLabel().equalsIgnoreCase("SetRedSpawn")){
			getConfig().set("RedSpawn.World", p.getWorld().getName().toString());
			getConfig().set("RedSpawn.X", p.getLocation().getBlockX());
			getConfig().set("RedSpawn.Y", p.getLocation().getBlockY());
			getConfig().set("RedSpawn.Z", p.getLocation().getBlockZ());
			saveConfig();
			p.sendMessage(ChatColor.GOLD + "Red Spawn has been set!");
			return true;
		}
		}
		
		if(p.hasPermission("CTF.Admin")){
		if(cmd.getLabel().equalsIgnoreCase("SetLobby")){
			getConfig().set("Lobby.World", p.getWorld().getName().toString());
			getConfig().set("Lobby.X", p.getLocation().getBlockX());
			getConfig().set("Lobby.Y", p.getLocation().getBlockY());
			getConfig().set("Lobby.Z", p.getLocation().getBlockZ());
			p.sendMessage(ChatColor.GOLD + "Lobby has been set!");
			saveConfig();
			return true;
		}
		}
		
		if(cmd.getLabel().equalsIgnoreCase("Test")){
			p.sendMessage(ChatColor.RED + "Red team: " + Game.RedScore);
			p.sendMessage(ChatColor.BLUE + "Blue team: " + Game.BlueScore);
		}
		return false;
	}
	
	public static void ScoreboardManager(){
		if(!(Game.hasStarted(true))){
		ScoreboardPreID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(CTF.instance, new Runnable() {
			
			@Override
			public void run() {
				for(Player p: Bukkit.getOnlinePlayers()){
					PregameScores.ScoreBoardPre(p);
				
				}
				
			}
		}, 0, 20);
		}
	}
	
}
