package com.src.classes;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.src.Game;
import com.src.GameState;
import com.src.utils.ActionbarTitleObject;

public class Classes implements Listener{
	
	public static List<UUID> Warrior = new ArrayList<UUID>();
	public static List<UUID> Archer = new ArrayList<UUID>();
	public static List<String> WarriorLore = new ArrayList<String>();
	public static List<String> ArcherLore = new ArrayList<String>();

	@EventHandler
	public static void onPlayerClick(PlayerInteractEvent e){
		
		Action a = e.getAction();
		if(!(Game.hasStarted(true))){
			if(GameState.isState(GameState.IN_LOBBY)){
		if(a == Action.RIGHT_CLICK_AIR && e.getPlayer().getItemInHand().getType() == Material.BOW){
			
			oGUI(e.getPlayer());
			
		}
		}
		}
		
	}
	
	private static void oGUI(Player p){
		
		Inventory Class = Bukkit.createInventory(null, 9,ChatColor.RED + "[CTF]" + ChatColor.BLUE + " Kits!");
		
		ItemStack Warrior = new ItemStack(Material.IRON_SWORD);
		ItemMeta WarriorMeta = Warrior.getItemMeta();
		
		WarriorMeta.setDisplayName(ChatColor.GREEN + "Warrior");
		WarriorMeta.setLore(WarriorLore);
		Warrior.setItemMeta(WarriorMeta);
		
		ItemStack Archer = new ItemStack(Material.BOW);
		ItemMeta ArcherMeta = Archer.getItemMeta();
		
		ArcherMeta.setDisplayName(ChatColor.GREEN + "Archer");
		ArcherMeta.setLore(ArcherLore);
		Archer.setItemMeta(ArcherMeta);
		
		Class.setItem(3, Warrior);
		Class.setItem(5, Archer);
		
		p.openInventory(Class);
		
	}
	
	@EventHandler
	public void onICE(InventoryClickEvent e){
		
		Player p = (Player) e.getWhoClicked();


		
	
		
		if(e.getInventory().getName().equals(ChatColor.RED + "[CTF]" + ChatColor.BLUE + " Kits!")){
			e.setCancelled(true);
			
			
			switch(e.getCurrentItem().getType()){
				case IRON_SWORD:
					if(!(Warrior.contains(p.getUniqueId()))){
					Warrior.add(p.getUniqueId());
					}
					if(Archer.contains(p.getUniqueId())){
						Archer.remove(p.getUniqueId());

					}
					p.closeInventory();
					new ActionbarTitleObject(ChatColor.GREEN + "Warrior Kit Selected").send(p);
					break;
				case BOW:
					if(!(Archer.contains(p.getUniqueId()))){
					Archer.add(p.getUniqueId());
					}
					if(Warrior.contains(p.getUniqueId())){
						Warrior.remove(p.getUniqueId());
					}
					p.closeInventory();
					new ActionbarTitleObject(ChatColor.GREEN + "Archer Kit Selected").send(p);
					break;
				default:
					break;
			}
			
		}
		
	}
	
	public static void Archer(Player p){
		p.getInventory().clear();
		
		ItemStack Bow = new ItemStack(Material.BOW);
		ItemMeta BowMeta = Bow.getItemMeta();
		
		BowMeta.setDisplayName(ChatColor.GREEN + "Archer's Bow");
		Bow.setItemMeta(BowMeta);
		Bow.addEnchantment(Enchantment.ARROW_INFINITE, 1);
		Bow.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
		
		ItemStack Arrow = new ItemStack(Material.ARROW);
		ItemMeta ArrowMeta = Arrow.getItemMeta();
		
		ArrowMeta.setDisplayName(ChatColor.GREEN + "Archer's Arrow");
		Arrow.setItemMeta(ArrowMeta);
		
		
		p.getInventory().addItem(Bow);
		p.getInventory().addItem(Arrow);
	}
	
	public static void Warrior(Player p){
		p.getInventory().clear();
		
		ItemStack Sword = new ItemStack(Material.IRON_SWORD);
		ItemMeta SwordMeta = Sword.getItemMeta();
		
		SwordMeta.setDisplayName(ChatColor.GREEN + "Warrior's Sword");
		Sword.setItemMeta(SwordMeta);
		Sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);

		
		ItemStack Dag = new ItemStack(Material.WOOD_SWORD);
		ItemMeta DagMeta = Dag.getItemMeta();
		
		DagMeta.setDisplayName(ChatColor.GREEN + "Warrior's Dagger");
		Dag.setItemMeta(DagMeta);
		Dag.addEnchantment(Enchantment.DAMAGE_ALL, 3);
		
		p.getInventory().addItem(Sword);
		p.getInventory().addItem(Dag);
		
	}
	
	public static void inLobbyKit(Player p){
		
		p.setLevel(15);
		p.setExp(1.0f);
		
		p.getInventory().clear();
    	p.getInventory().setHelmet(new ItemStack(Material.AIR));
		
		ItemStack Menu = new ItemStack(Material.BOW);
		ItemMeta MenuMeta = Menu.getItemMeta();
		
		MenuMeta.setDisplayName(ChatColor.GREEN + "Kit Selection");
		Menu.setItemMeta(MenuMeta);
		
		p.getInventory().addItem(Menu);
	}
	
	
}
