package com.src.teams;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Team {

	private static List<String> redTeam = new ArrayList<String>();
	private static List<String> blueTeam = new ArrayList<String>();
	private static List<String> spectatorTeam = new ArrayList<String>();

	private static List<String> combineTeams = new ArrayList<String>();

	
	public static void addToTeam(TeamType type, Player player){
		if(isOnTeam(player)){
			return;
		}
		switch(type){
		case RED:
			redTeam.add(player.getName());
			player.sendMessage(ChatColor.GREEN + "You have been added to " + type.toString() + " team!");
			break;
		case BLUE:
			blueTeam.add(player.getName());
			player.sendMessage(ChatColor.GREEN + "You have been added to " + type.toString() + " team!");
			break;
		case Spectator:
			spectatorTeam.add(player.getName());
			player.sendMessage(ChatColor.GREEN + "You are now a " + type.toString() + "!");
			break;
		}
	}

	public static boolean isOnTeam(Player player){
		return redTeam.contains(player.getName()) || blueTeam.contains(player.getName()) || spectatorTeam.contains(player.getName());
	}
	
	public static void clearTeams(){
		redTeam.clear();
		blueTeam.clear();
		spectatorTeam.clear();
	}
	
	public static List<String> getRedTeam(){
		return redTeam;
	}
	public static List<String> getBlueTeam(){
		return blueTeam;
	}
	public static List<String> getSpectatorTeam(){
		return spectatorTeam;
	}
	
	public static List<String> getAllPlayersOnTeams(){

		combineTeams.addAll(redTeam);
		combineTeams.addAll(blueTeam);
		return combineTeams;

	}
	
	public static TeamType getTeamType(Player player){
		
		
		if(!(isOnTeam(player))){
			return null;
		}
		
		return(redTeam.contains(player.getName()) ? TeamType.RED : TeamType.BLUE);
	}
	
	public static TeamType getIfSpectatorTeam(Player player){	
		
		if(spectatorTeam.contains(player.getName())){
			return TeamType.Spectator;
		}
		return null;
		
	}
	
}
