package com.src;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.src.classes.Classes;
import com.src.scoreboards.DuringGameSB;
import com.src.scoreboards.PregameScores;
import com.src.teams.Team;
import com.src.teams.TeamType;
import com.src.threads.StartCountdown;
import com.src.utils.ActionbarTitleObject;
import com.src.utils.LocationUtils;
import com.src.utils.TitleObject;

public class Game {

	 private static boolean canStart = false;
	 private static boolean hasStarted = false;
	 public static int RedScore = 0;
	 public static int BlueScore = 0;
	 public static Location RedFlag;
	 public static Location BlueFlag;
	 static CTF plugin;
	 
	 
	 
	 @SuppressWarnings("deprecation")
	public static void start(){
		 
		 GameState.setState(GameState.IN_GAME);
		 setHasStarted(true);
		 int i = 0;
		 for(Player p : Bukkit.getOnlinePlayers()){
			 p.getInventory().clear();
			 DuringGameSB.onGameStartProgress(p);
			 if(i < Bukkit.getOnlinePlayers().size() / 2){
				 Team.addToTeam(TeamType.RED, p);
			 }else{
				 Team.addToTeam(TeamType.BLUE, p);
			 }
			 
			 if(Classes.Archer.contains(p.getUniqueId())){
				 Classes.Archer(p);
			 }
			 if(Classes.Warrior.contains(p.getUniqueId())){
				 Classes.Warrior(p);
			 }
			 
			 if(!(Classes.Archer.contains(p.getUniqueId()))){
				 if(!(Classes.Warrior.contains(p.getUniqueId()))){
				 Classes.Warrior.add(p.getUniqueId());
				 Classes.Warrior(p);
				 }
			 }
			 
			 teleportToTeamSpawn(p);
			 
			 if(Team.getTeamType(p).equals(TeamType.RED)){
				 p.getInventory().setHelmet(new ItemStack(Material.WOOL, 1, DyeColor.RED.getData()));
					new TitleObject(ChatColor.GREEN + "Game has Started!", " ").setFadeIn(10).setStay(20).setFadeOut(10).send(p);
					new ActionbarTitleObject(ChatColor.RED + "Red team!");

			 }
			 if(Team.getTeamType(p).equals(TeamType.BLUE)){
				 p.getInventory().setHelmet(new ItemStack(Material.WOOL, 1, DyeColor.BLUE.getData()));
					new TitleObject(ChatColor.GREEN + "Game has Started!", " ").setFadeIn(10).setStay(20).setFadeOut(10).send(p);
					new ActionbarTitleObject(ChatColor.BLUE + "Blue team!");
			 }
			 
			 
			 if(Team.getTeamType(p).equals(TeamType.Spectator)){
			 Spectator(p);
			 }
			 
			 
			 i++; 
		 }
		 
	 }
	 
	 @SuppressWarnings("deprecation")
	public static void respawnRedFlag(){
		 
		 if(RedFlag == null){
			 return;
		 }
		 
		 RedFlag.getBlock().setType(Material.WOOL);
		 RedFlag.getBlock().setData(DyeColor.RED.getData());
		 for(Player pl : Bukkit.getOnlinePlayers()){
		 pl.playEffect(RedFlag.add(0, 0, 0), Effect.ENDER_SIGNAL, 20);
		 pl.playSound(RedFlag.add(0, 1, 0), Sound.ANVIL_LAND, 20f, 20f);
		 }
		 
	 }
	 
	 @SuppressWarnings("deprecation")
	public static void respawnBlueFlag(){
		 
		 if(BlueFlag == null){
			 return;
		 }
		 BlueFlag.getBlock().setType(Material.WOOL);
		 BlueFlag.getBlock().setData(DyeColor.BLUE.getData());
		 for(Player pl : Bukkit.getOnlinePlayers()){
		 pl.playEffect(BlueFlag.add(0, 0, 0), Effect.ENDER_SIGNAL, 20);
		 pl.playSound(RedFlag.add(0, 1, 0), Sound.ANVIL_LAND, 20f, 20f);
		 }
		 
	 }
	 
	 public static void stop(){
		 
		 GameState.setState(GameState.RESETTING);
		 resetGame();
	 }
	 
	 public static void RedTeamWins(){
		 GameState.setState(GameState.POST_GAME);
	 }
	 
	public static void resetGame(){
		 
		 GameState.setState(GameState.RESETTING);
		 
		 for(Player p : Bukkit.getOnlinePlayers()){
			 p.teleport(LocationUtils.Lobby);
			 p.getInventory().clear();
			 PregameScores.ScoreBoardPre(p);
			 p.updateInventory();
				p.setLevel(15);
				p.setExp(1.0f);
		 }
		 
		 GameState.setState(GameState.IN_LOBBY);
		 
		setRedScore(0);
		setBlueScore(0);
		Game.setHasStarted(false);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(CTF.instance, new Runnable() {
			
			@Override
			public void run() {
				StartCountdown.timeUntilStart = 15;
				CTF.startCountdownID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(CTF.instance, new StartCountdown(plugin), 20l, 20l);
				CTF.ScoreboardManager();
			}
		}, 20*5);
		
		Classes.Archer.clear();
		Classes.Warrior.clear();
		
        for(Player p : Bukkit.getOnlinePlayers()){        	
        	Classes.inLobbyKit(p);
        }
		 
	 }
	 
	public static void stopCountdown(){
			Bukkit.getServer().getScheduler().cancelTask(CTF.startCountdownID);
	}
	 
	public static void resetCountdown(){
		stopCountdown();
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(CTF.instance, new Runnable() {
			
			@Override
			public void run() {
				StartCountdown.timeUntilStart = 15;
				CTF.startCountdownID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(CTF.instance, new StartCountdown(plugin), 20l, 20l);
			}
		}, 20*5);
	}
	
	public static void Spectator(Player p){
		
		for(Player pl : Bukkit.getOnlinePlayers()){
			pl.hidePlayer(p);
		}
		
		
	}
	
	public static void teleportToTeamSpawn(Player p){
		if(Team.getTeamType(p).equals(TeamType.RED)){
			p.teleport(LocationUtils.RedSpawn);
		}
		if(Team.getTeamType(p).equals(TeamType.BLUE)){
			p.teleport(LocationUtils.BlueSpawn);
		}
	}
	
	
	 public static void setRedScore(int i){
		 RedScore = i;
	 }
	 
	 public static void setBlueScore(int i){
		 BlueScore = i;
	 }
	 
	  public static boolean canStart()
	  {
	    return canStart;
	  }

	  public static void setCanStart(boolean b)
	  {
	    canStart = b;
	  }

	  public static void stop(Team team)
	  {
	    hasStarted = false;
	  }

	  public static boolean hasStarted()
	  {
	    return hasStarted;
	  }

	  public static void setHasStarted(boolean b)
	  {
	    hasStarted = b;
	  }

}
