package com.src.threads;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.src.CTF;
import com.src.Game;
import com.src.utils.ChatUtilities;
import com.src.utils.TitleObject;

public class StartCountdown implements Runnable {
	
	CTF plugin;

	public static int timeUntilStart;
	
	public StartCountdown(CTF pl){
		plugin = pl;
	}

	public void run() {
		if(Bukkit.getOnlinePlayers().size() >= 2){
			Game.setCanStart(true);
		}
		if(Bukkit.getOnlinePlayers().size() < 2){
			Game.setCanStart(false);
		}
		
		if(Game.canStart()){
				for(Player p : Bukkit.getOnlinePlayers()){
				if(timeUntilStart == 4){
					new TitleObject(ChatColor.GREEN + "3", " ").setFadeIn(1).setStay(10).setFadeOut(1).send(p);
					p.playSound(p.getLocation(), Sound.LEVEL_UP, 20f, 20f);
				}
				if(timeUntilStart == 3){
					new TitleObject(ChatColor.YELLOW + "2", " ").setFadeIn(1).setStay(10).setFadeOut(1).send(p);
					p.playSound(p.getLocation(), Sound.LEVEL_UP, 20f, 20f);
				}
				if(timeUntilStart == 2){
					new TitleObject(ChatColor.RED + "1", " ").setFadeIn(1).setStay(10).setFadeOut(1).send(p);
					p.playSound(p.getLocation(), Sound.LEVEL_UP, 20f, 20f);
				}
				p.setLevel(p.getLevel() - 1);
				p.setExp(p.getExp() - 0.06666667f);
				}
				if(timeUntilStart == 0){
					if(Game.canStart()){
					Game.stopCountdown();
					Bukkit.getServer().getScheduler().cancelTask(CTF.ScoreboardPreID);
					Game.start();
					Game.stopCountdown();
					ChatUtilities.broadcast(ChatColor.GREEN + "Game Starting!");
					
					}
					
				}
				
				timeUntilStart -= 1;
			}
}
}
