package com.src.scoreboards;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import com.src.Game;

public class DuringGameSB {
	
	public static void onGameInProgress(Player p){
		
		Scoreboard game = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective DuringGame = game.registerNewObjective("DuringGame", "DuringGame");
		
		DuringGame.setDisplaySlot(DisplaySlot.SIDEBAR);
		DuringGame.setDisplayName(ChatColor.WHITE + "Capture The Flag!");
		
		Score Blank1 = DuringGame.getScore(ChatColor.AQUA + " ");
		String pName = ChatColor.BLUE + "Players";
		Score playersName = DuringGame.getScore(pName);
		Score players = DuringGame.getScore(ChatColor.WHITE + "" + PregameScores.i + "     ");
		Score Blank2 = DuringGame.getScore(ChatColor.GOLD + " ");
		Score RedName = DuringGame.getScore(ChatColor.RED + "Red Score");
		Score RedScore = DuringGame.getScore(ChatColor.WHITE +  "" + Game.RedScore);
		Score Blank3 = DuringGame.getScore(ChatColor.BLUE + " ");
		Score Bluename = DuringGame.getScore(ChatColor.BLUE + "Blue Score");
		Score BlueScore = DuringGame.getScore(ChatColor.WHITE + "" + Game.BlueScore);
		
		Blank1.setScore(8);
		playersName.setScore(7);
		players.setScore(6);
		Blank2.setScore(5);
		RedName.setScore(4);
		RedScore.setScore(3);
		Blank3.setScore(2);
		Bluename.setScore(1);
		BlueScore.setScore(0);
		
		p.setScoreboard(game);
	}
	
	public static void onGameStartProgress(Player p){
		
		Scoreboard game = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective DuringGame = game.registerNewObjective("DuringGame", "DuringGame");
		
		DuringGame.setDisplaySlot(DisplaySlot.SIDEBAR);
		DuringGame.setDisplayName(ChatColor.WHITE + "Capture The Flag!");
		
		Score Blank1 = DuringGame.getScore(ChatColor.AQUA + " ");
		String pName = ChatColor.BLUE + "Players";
		Score playersName = DuringGame.getScore(pName);
		Score players = DuringGame.getScore(ChatColor.WHITE + "" + PregameScores.i + "     ");
		Score Blank2 = DuringGame.getScore(ChatColor.GOLD + " ");
		Score RedName = DuringGame.getScore(ChatColor.RED + "Red Score");
		Score RedScore = DuringGame.getScore(ChatColor.WHITE + "0     ");
		Score Blank3 = DuringGame.getScore(ChatColor.BLUE + " ");
		Score Bluename = DuringGame.getScore(ChatColor.BLUE + "Blue Score");
		Score BlueScore = DuringGame.getScore(ChatColor.WHITE + "0");
		
		Blank1.setScore(8);
		playersName.setScore(7);
		players.setScore(6);
		Blank2.setScore(5);
		RedName.setScore(4);
		RedScore.setScore(3);
		Blank3.setScore(2);
		Bluename.setScore(1);
		BlueScore.setScore(0);
		
		p.setScoreboard(game);
	}
}
