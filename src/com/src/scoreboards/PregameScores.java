package com.src.scoreboards;

import static org.bukkit.ChatColor.BLUE;
import static org.bukkit.ChatColor.RED;
import static org.bukkit.ChatColor.WHITE;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import com.src.threads.StartCountdown;

public class PregameScores {


		public static Scoreboard pregame;
		private static Objective PreGame;
		private static Score playersName;
		private static Score players;
		private static Score gamename;
		private static Score gameTime;
		
		public static int i;

		public static void ScoreBoardPre(Player p){
			pregame = Bukkit.getScoreboardManager().getNewScoreboard();
			PreGame = pregame.registerNewObjective("CTF", "CTF");
			
			PreGame.setDisplaySlot(DisplaySlot.SIDEBAR);
			PreGame.setDisplayName(ChatColor.WHITE + "Capture The Flag!");
			
			Score blank1 = PreGame.getScore(RED + " ");
			String pName = BLUE + "Players      ";
			playersName = PreGame.getScore(pName);
			players = PreGame.getScore(WHITE + "" + i + "");
			Score blank2 = PreGame.getScore(RED + " ");
			String gName = BLUE + "Starts in:";
			gamename = PreGame.getScore(gName);
			gameTime = PreGame.getScore(WHITE + "" + StartCountdown.timeUntilStart + "");
			
			blank1.setScore(8);
			playersName.setScore(7);
			players.setScore(6);
			blank2.setScore(5);
			gamename.setScore(4);
			gameTime.setScore(3);
			
			p.setScoreboard(pregame);
	}
	
}
