package com.src.events;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.src.Game;
import com.src.teams.Team;

public class PlayerDamage implements Listener {

	@EventHandler
	public void onPlayerDamage(EntityDamageByEntityEvent e) {

		if (!(Game.hasStarted(true))) {
			e.setCancelled(false);
		}

		if (Game.hasStarted(true)) {

			if (e.getDamager() instanceof Arrow) {
				Arrow a = (Arrow) e.getDamager();

				if (a.getShooter() instanceof Player) {

					Player shooter = (Player) a.getShooter();
					if (e.getEntity() instanceof Player) {
						Player p = (Player) e.getEntity();
						if (Team.getTeamType(shooter) == Team.getTeamType(p)) {
							e.setCancelled(true);
						}
					}
				}
				return;
			}
			if (!(e.getEntity() instanceof Player))
				return;
			Player attacker = (Player) e.getDamager();
			if (e.getEntity() instanceof Player) {
				Player p = (Player) e.getEntity();
				if (Team.getTeamType(attacker) == Team.getTeamType(p)) {
					e.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void repairWeapons(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player) {
			((Player) e.getDamager()).getInventory().getItemInHand()
					.setDurability((short) -1);
		} else if (e.getEntity() instanceof Player) {
			ItemStack[] armor = ((Player) e.getEntity()).getInventory()
					.getArmorContents();
			for (ItemStack i : armor) {
				i.setDurability((short) 0);
			}
		}
	}

	@EventHandler
	public void repairBow(EntityShootBowEvent e) {
		if (e.getEntity() instanceof Player) {
			e.getBow().setDurability((short) -1);
		}
	}

	@EventHandler
	public void onItemBreakDamage(PlayerItemBreakEvent e) {
		ItemStack item = new ItemStack(e.getBrokenItem().getType());
		e.getPlayer().getInventory().addItem(item);
	}

	@EventHandler
	public void onPlayerBreakBlock(BlockBreakEvent e) {

		e.setCancelled(true);

	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		e.setCancelled(true);
	}
}
