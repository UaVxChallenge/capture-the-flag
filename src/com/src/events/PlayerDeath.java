package com.src.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

import com.src.Game;
import com.src.utils.ChatUtilities;

public class PlayerDeath implements Listener{

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e){
		
		e.getDrops().clear();
		
		if(!(e.getEntity() instanceof Player)){
			return;
		}
		
		Player p = e.getEntity();
		
		if(Game.hasStarted(true)){
			
			if(p.getInventory().contains(Material.WOOL)){
				
				ItemStack redWool = new ItemStack(Material.WOOL, 1, DyeColor.RED.getData());
				ItemStack blueWool = new ItemStack(Material.WOOL, 1, DyeColor.BLUE.getData());
				
				if(p.getInventory().contains(redWool)){
					Game.respawnRedFlag();
					Bukkit.getServer().getScheduler().cancelTask(RightClickBlock.FireworkBlue);
					ChatUtilities.broadcast("Player with Red teams flag has died!");
				}
				
				if(p.getInventory().contains(blueWool)){
					Game.respawnBlueFlag();
					Bukkit.getServer().getScheduler().cancelTask(RightClickBlock.FireworkRed);

					ChatUtilities.broadcast(ChatColor.BLUE + "Player with Blue teams flag has died!");
				}
				
			}
			
			p.getInventory().clear();
			
			
			
		}
		
	}
	
	@EventHandler
	public void onItemPickUp(PlayerPickupItemEvent e){
		e.setCancelled(true);
	}
}
