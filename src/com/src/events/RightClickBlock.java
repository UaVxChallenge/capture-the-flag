package com.src.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;

import com.src.CTF;
import com.src.Game;
import com.src.classes.Classes;
import com.src.scoreboards.DuringGameSB;
import com.src.teams.Team;
import com.src.teams.TeamType;
import com.src.utils.ActionbarTitleObject;
import com.src.utils.ChatUtilities;
import com.src.utils.TitleObject;

public class RightClickBlock implements Listener{
	
	public static int FireworkRed;
	public static int FireworkBlue;
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onRightClick(PlayerInteractEvent e){
		
		
		Player p = (Player) e.getPlayer();

		if(!(e.getAction() == Action.RIGHT_CLICK_BLOCK)) return;
		
		if(Game.hasStarted()){
			
		ItemStack redWool = new ItemStack(Material.WOOL, 1, DyeColor.RED.getData());
		ItemStack blueWool = new ItemStack(Material.WOOL, 1, DyeColor.BLUE.getData());

		if(Team.getTeamType(p).equals(TeamType.BLUE)){
			
			if(e.getClickedBlock().getType() != Material.WOOL) return;
			
			
			if(e.getClickedBlock().getType() == Material.WOOL){
				if(e.getClickedBlock().getData() == DyeColor.RED.getData()){
					Game.RedFlag = e.getClickedBlock().getLocation();
					
					for(Player pl: Bukkit.getOnlinePlayers()){
						new ActionbarTitleObject(ChatColor.RED + "Red wool has been taken!").send(pl);
					}
			p.getInventory().clear();
			p.updateInventory();
			for(int j = 0; j <= 35; j++){
				p.getInventory().setItem(j, redWool);
			}
			doFireworkBlue(p);
			p.updateInventory();
			e.getClickedBlock().setType(Material.AIR);
				}
			}
		}
		
		if(Team.getTeamType(p).equals(TeamType.RED)){
			if(e.getClickedBlock().getType() == Material.WOOL){
				if(e.getClickedBlock().getData() == DyeColor.BLUE.getData()){
					Game.BlueFlag = e.getClickedBlock().getLocation();
			p.getInventory().clear();
			p.updateInventory();
			for(int j = 0; j <= 35; j++){
				p.getInventory().setItem(j, blueWool);
			}
			p.updateInventory();
			doFireworkRed(p);
			for(Player pl: Bukkit.getOnlinePlayers()){
				new ActionbarTitleObject(ChatColor.RED + "Blue wool has been taken!").send(pl);
			}
			e.getClickedBlock().setType(Material.AIR);
				}
			}
		}
	}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e){

		Player p = e.getPlayer();
		
		if(Team.getSpectatorTeam().contains(p.getUniqueId())){
		for(Player pl :Bukkit.getOnlinePlayers()){
			pl.hidePlayer(e.getPlayer());
		}
		}
		
		if(!(p.getInventory().contains(Material.WOOL))) return;
		if(Game.hasStarted(true)){
			
		Block b = e.getPlayer().getLocation().getBlock();
		Location loc = b.getLocation();
		double X = loc.getBlockX();
		double Y = loc.getBlockY();
		double Z = loc.getBlockZ();

		int radius = 5 - 2;
		double minX = X - radius;
		double maxX = X + radius + 1.0D;
		double minY = Y - radius;
		double maxY = Y + radius + 1.0D;
		double minZ = Z - radius;
		double maxZ = Z + radius + 1.0D;
		for (double x = minX; x < maxX; x += 1.0D) {
			for (double y = minY; y < maxY; y += 1.0D)
				for (double z = minZ; z < maxZ; z += 1.0D) {
					Location location = new Location(b.getWorld(), x,
							y, z);
					if(Team.getTeamType(p).equals(TeamType.RED)){
					if(location.getBlock().getType() == Material.WOOL && location.getBlock().getData() == DyeColor.RED.getData()){
						if(p.getInventory().contains(new ItemStack(Material.WOOL, 1, DyeColor.BLUE.getData()))){
							Bukkit.getServer().getScheduler().cancelTask(FireworkRed);
						p.getPlayer().getInventory().clear();
						p.updateInventory();
						if(Classes.Archer.contains(p.getUniqueId())){
							Classes.Archer(p);
						}
						if(Classes.Warrior.contains(p.getUniqueId())){
							Classes.Warrior(p);
						}
						ChatUtilities.broadcast(ChatColor.RED + "Red Team has captured a flag!");
						Game.setRedScore(Game.RedScore + 1);
						Game.respawnBlueFlag();
						for(Player pl : Bukkit.getOnlinePlayers()){
							DuringGameSB.onGameInProgress(pl);
						}
						if(Game.RedScore == 3){
							Game.setHasStarted(false);
							for(Player pl : Bukkit.getOnlinePlayers()){
							new TitleObject(ChatColor.RED + "Game Over", ChatColor.RED + "Red team has won!").setFadeIn(20).setStay(20).setFadeOut(20).send(pl);
							}
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(CTF.instance, new Runnable() {
								
								@Override
								public void run() {
									Game.resetGame();
									
								}
							}, 20*10);
							
						}
						}
					}
					}
					if(Team.getTeamType(p).equals(TeamType.BLUE)){
					if(location.getBlock().getType() == Material.WOOL && location.getBlock().getData() == DyeColor.BLUE.getData()){
						if(p.getInventory().contains(new ItemStack(Material.WOOL, 1, DyeColor.RED.getData()))){
							Bukkit.getServer().getScheduler().cancelTask(FireworkBlue);
						p.getPlayer().getInventory().clear();
						p.updateInventory();
						if(Classes.Archer.contains(p.getUniqueId())){
							Classes.Archer(p);
						}
						if(Classes.Warrior.contains(p.getUniqueId())){
							Classes.Warrior(p);
						}
						ChatUtilities.broadcast(ChatColor.BLUE + "Blue Team has captured a flag!");
						Game.setBlueScore(Game.BlueScore + 1);
						Game.respawnRedFlag();
						for(Player pl : Bukkit.getOnlinePlayers()){
							DuringGameSB.onGameInProgress(pl);
						}
						if(Game.BlueScore == 3){
							Game.setHasStarted(false);
							for(Player pl : Bukkit.getOnlinePlayers()){
								new TitleObject(ChatColor.RED + "Game Over", ChatColor.BLUE + "Blue team has won!").setFadeIn(20).setStay(20).setFadeOut(20).send(pl);
								}
							Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(CTF.instance, new Runnable() {
								
								@Override
								public void run() {
									Game.resetGame();
									
								}
							}, 20*10);
						}
					}
					}
					}
	}
		}
		}
	}
	
	public static void doFireworkBlue(final Player p){
		FireworkBlue = Bukkit.getScheduler().scheduleSyncRepeatingTask(CTF.instance, new Runnable() {
			
			@Override
			public void run() {
					if(Team.getTeamType(p).equals(TeamType.BLUE)){
					Firework f = (Firework) p.getWorld().spawn(p.getLocation(), Firework.class);
			        
			        FireworkMeta fm = f.getFireworkMeta();
			        fm.addEffect(FireworkEffect.builder()
			                        .flicker(false)
			                        .trail(true)
			                        .with(Type.CREEPER)
			                        .withColor(Color.BLUE)
			                        .withFade(Color.BLUE)
			                        .build());
			        fm.setPower(3);
			        f.setFireworkMeta(fm);
					}
			}
		}, 0l, 20*5l);
	}
	
	public static void doFireworkRed(final Player p){
		FireworkRed = Bukkit.getScheduler().scheduleSyncRepeatingTask(CTF.instance, new Runnable() {
			
			@Override
			public void run() {
				if(Team.getTeamType(p).equals(TeamType.RED)){
					Firework f = (Firework) p.getWorld().spawn(p.getLocation(), Firework.class);
			        
			        FireworkMeta fm = f.getFireworkMeta();
			        fm.addEffect(FireworkEffect.builder()
			                        .flicker(false)
			                        .trail(true)
			                        .with(Type.CREEPER)
			                        .withColor(Color.RED)
			                        .withFade(Color.RED)
			                        .build());
			        fm.setPower(3);
			        f.setFireworkMeta(fm);
					}
			}
		}, 0l, 20*5l);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onPlayerDropItems(PlayerDropItemEvent e){
		e.setCancelled(true);
	}
}
