package com.src.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.src.CTF;
import com.src.Game;
import com.src.GameState;
import com.src.classes.Classes;
import com.src.scoreboards.PregameScores;
import com.src.teams.Team;
import com.src.teams.TeamType;
import com.src.utils.LocationUtils;

public class PlayerJoin implements Listener{
	
	@EventHandler
	public void onPLJ(PlayerJoinEvent e){
		
		PregameScores.i = PregameScores.i +1;
		
		for(Player p: Bukkit.getOnlinePlayers()){
			PregameScores.ScoreBoardPre(p);
		}
		
		if(GameState.isState(GameState.IN_LOBBY)){
			Classes.inLobbyKit(e.getPlayer());
			}
		
		Player p = (Player) e.getPlayer();
		
		if(GameState.isState(GameState.IN_GAME)){
			Team.addToTeam(TeamType.Spectator, p);
			
			if(Team.getTeamType(p).equals(TeamType.Spectator)){
				Game.Spectator(p);
			}
			
		}
		
		if(LocationUtils.Lobby != null){
		e.getPlayer().teleport(LocationUtils.Lobby);
		}else{
			return;
		}

	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e){
		PregameScores.i = PregameScores.i - 1;
		
		for(Player p: Bukkit.getOnlinePlayers()){
			PregameScores.ScoreBoardPre(p);
		}
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e){
		
		final Player p = e.getPlayer();
		if(Game.hasStarted(true)){
		if(Team.getTeamType(p).equals(TeamType.RED)){
			Bukkit.getScheduler().scheduleSyncDelayedTask(CTF.instance, new Runnable() {
				
				@Override
				public void run() {
					p.teleport(LocationUtils.RedSpawn);
					
				}
			}, 5);
		}
		
		if(Team.getTeamType(p).equals(TeamType.BLUE)){
			Bukkit.getScheduler().scheduleSyncDelayedTask(CTF.instance, new Runnable() {
				
				@Override
				public void run() {
					p.teleport(LocationUtils.BlueSpawn);
					
				}
			}, 5);
		}
		
		if(Classes.Archer.contains(p.getUniqueId())){
			Classes.Archer(p);
		}
		
		if(Classes.Warrior.contains(p.getUniqueId())){
			Classes.Warrior(p);
		}
		}
		
	}
	

}
